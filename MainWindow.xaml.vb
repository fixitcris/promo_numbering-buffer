﻿Imports System.IO
Imports System.ComponentModel
Imports System.Text
Imports System.Net.Sockets
Imports System.Security.Cryptography.X509Certificates

Class MainWindow
    Dim p1type, p2type, p3type, p4type, p1IPstr, p2IPstr, p3IPstr, p4IPstr, p1PORTstr, p2PORTstr, p3PORTstr, p4PORTstr, DFile, DFile2, next_message, message, P_ACKstr, buffer_count, length As String
    Dim request As String = "{~EC|External_0|}"
    Dim Buffer As New List(Of String)
    Dim DBList As New List(Of String)
    Dim NGPCL As New TcpClient
    Dim NGPCL2 As New TcpClient
    Dim P_ACK() As Byte
    Dim Remainder, CodeCount, b_count, Low, filepos, bufferlength As Integer
    Dim ConnectError, CommsFault, firstload, Change As Boolean
    Private WithEvents Running As New BackgroundWorker
    Private WithEvents Monthly_download As New BackgroundWorker
    Private WithEvents _94xxBuilder As New BackgroundWorker
    Private WithEvents NGPCLBuilder As New BackgroundWorker
    Private WithEvents _90xxBuilder As New BackgroundWorker
    Public BlackBrush As New SolidColorBrush With {.Color = Colors.Black}
    Public OrangeBrush As New SolidColorBrush With {.Color = Colors.Orange}
    Public GreenBrush As New SolidColorBrush With {.Color = Colors.LimeGreen}
    Public RedBrush As New SolidColorBrush With {.Color = Colors.Red}
    Public DB_Path As String
    Public Source_Folder As Process
    Private Sub MainWindow_Initialized(sender As Object, e As EventArgs) Handles Me.Initialized
        If Not My.Settings.IUpgraded Then 'Upgrade application settings from previous version
            My.Settings.Upgrade()
            If Not My.Settings.IUpgraded Then 'enumerate AppData folder to find previous versions
                Dim lastVersion As String = "1.0.0.2" 'version to upgrade settings from
                Dim config_initial As System.Configuration.Configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal)
                Dim fpath As String = config_initial.FilePath
                For x = 1 To 3 'recurse backwards to find root CompanyName folder
                    fpath = fpath.Substring(0, InStrRev(fpath, "\", Len(fpath) - 1))
                Next
                fpath = fpath.Substring(0, Len(fpath) - 1) 'remove trailing backslash
                Dim latestConfig As FileInfo 'If not set then no previous info found
                Dim di As DirectoryInfo = New DirectoryInfo(fpath)
                If di.Exists Then
                    For Each diSubDir As DirectoryInfo In di.GetDirectories(lastVersion, SearchOption.AllDirectories)
                        If InStr(diSubDir.FullName, ".vshost") = 0 Then 'don't find VS runtime copies
                            Dim files() As FileInfo = diSubDir.GetFiles("user.config", SearchOption.TopDirectoryOnly)
                            For Each File As FileInfo In files
                                Try
                                    If File.LastWriteTime > latestConfig.LastWriteTime Then
                                        latestConfig = File
                                    End If
                                Catch
                                    latestConfig = File
                                End Try
                            Next
                        End If
                    Next
                End If
                Try
                    If latestConfig.Exists Then
                        Dim newPath As String = config_initial.FilePath
                        newPath = newPath.Substring(0, InStrRev(newPath, "\", Len(newPath) - 1))
                        newPath = newPath.Substring(0, InStrRev(newPath, "\", Len(newPath) - 1))
                        newPath &= lastVersion
                        If Directory.Exists(newPath) = False Then
                            Directory.CreateDirectory(newPath)
                        End If
                        latestConfig.CopyTo(newPath & "\user.config")
                        My.Settings.Upgrade() 'Try upgrading again now old user.config exists in correct place
                    End If
                Catch : End Try
            End If
            My.Settings.IUpgraded = True 'Always set this to avoid potential upgrade loop
            My.Settings.Save()
        End If
        DB_Path = File.ReadAllText("C:\Program Files\Mitech\MiPromo\Data Store\Source.mit")
        Console.WriteLine("PATH = " & DB_Path)
        firstload = True
        For Each file As String In System.IO.Directory.GetFiles(DB_Path)
            DataFile.Items.Add(System.IO.Path.GetFileName(file))
        Next
        For i = 0 To DataFile.Items.Count - 1
            DataFile_2.Items.Add(DataFile.Items(i))
        Next
        Printed_.Text = "0"
        Remaining_.Text = "0"
        DBList.Clear()
        Buffer.Clear()
    End Sub
    Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Version.Text = "Ver " & My.Application.Info.Version.ToString
        Dim NOP As Integer = My.Settings.No_Of_Printers
        If NOP = 1 Then
            Printer2.Visibility = 1
            p2IP.Visibility = 1
            P2ipLabel.Visibility = 1
            p2PORT.Visibility = 1
            p2portLabel.Visibility = 1
            Start_2.Visibility = 1
            StopB_2.Visibility = 1
            DataFile_2.Visibility = 1
            P2BG.Visibility = 1
            P2_Border.Visibility = 1
            DF_label2.Visibility = 1
            DF_label_2.Visibility = 1
            label2.Visibility = 1
            label1.Margin = New Thickness(53 + 196, 4, 0, 0)
            P1border.Margin = New Thickness(16 + 196, 4, 0, 0)
            Printer1.Margin = New Thickness(55 + 196, 38, 0, 0)
            p1IP.Margin = New Thickness(53 + 196, 86, 0, 0)
            p1label.Margin = New Thickness(55 + 196, 39, 0, 0)
            p1PORT.Margin = New Thickness(53 + 196, 143, 0, 0)
            p1portLabel.Margin = New Thickness(78 + 196, 42, 0, 0)
            Start.Margin = New Thickness(36 + 196, 464, 0, 0)
            StopB.Margin = New Thickness(36 + 196, 588, 0, 0)
            DataFile.Margin = New Thickness(36 + 196, 224, 0, 0)
            P1BG.Margin = New Thickness(21 + 196, 9, 0, 0)
            P1border.Margin = New Thickness(16 + 196, 4, 0, 0)
            DF_label.Margin = New Thickness(36 + 196, 239, 0, 0)
            DF_label_Copy.Margin = New Thickness(36 + 196, 192, 0, 0)
        End If
        Console.WriteLine("File Position: " & My.Settings.FilePos)
        Running.WorkerSupportsCancellation = True
        Monthly_download.WorkerSupportsCancellation = True
        _94xxBuilder.WorkerSupportsCancellation = True
        NGPCLBuilder.WorkerSupportsCancellation = True
        _90xxBuilder.WorkerSupportsCancellation = True
        StopB.IsEnabled = 0
        StopB_2.IsEnabled = 0
        Printer1.SelectedIndex = My.Settings.Printer1
        p1IP.Text = My.Settings.P1IP
        p1PORT.Text = My.Settings.P1PORT
        Printer2.SelectedIndex = My.Settings.Printer2
        p2IP.Text = My.Settings.P2IP
        p2PORT.Text = My.Settings.P2PORT
        filepos = My.Settings.FilePos
        Dim DBex As Boolean
        Try
            If Month(Now) < 10 Then
                DataFile.SelectedValue = "rfider-" & Year(Now) & "-0" & Month(Now) & ".csv"
            Else
                DataFile.SelectedValue = "rfider-" & Year(Now) & "-" & Month(Now) & ".csv"
            End If
            DataFile_2.SelectedIndex = DataFile.SelectedIndex
            CodeCount = (File.ReadAllLines(DB_Path & DFile).Length)
        Catch ex As Exception
            DBex = True
            MsgBox("Correct Datafile not found, please select manually")
            DataFile.IsEnabled = 1
        End Try
        filepos = My.Settings.FilePos
        If DBex = False Then
            READER()
            Remaining_.Text = CodeCount - filepos
            New_Batch()
        End If
        Monthly_download.RunWorkerAsync()
        'Message_Name.Text = My.Settings.Message
        If p1IP.Text IsNot Nothing Then
            P1ipLabel.Opacity = 0
        End If
        If p1PORT.Text IsNot Nothing Then
            p1portLabel.Opacity = 0
        End If
        If p2IP.Text IsNot Nothing Then
            P2ipLabel.Opacity = 0
        End If
        If p2PORT.Text IsNot Nothing Then
            p2portLabel.Opacity = 0
        End If
        If p3IP.Text IsNot Nothing Then
            P1ipLabel.Opacity = 0
        End If
        If p3PORT.Text IsNot Nothing Then
            p3portLabel.Opacity = 0
        End If
        If p4IP.Text IsNot Nothing Then
            P4ipLabel.Opacity = 0
        End If
        If p4PORT.Text IsNot Nothing Then
            p4portLabel.Opacity = 0
        End If
        If DataFile.SelectedValue IsNot Nothing Then
            DF_label.Opacity = 0
        End If
    End Sub
    Private Sub Printer1_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles Printer1.SelectionChanged, Printer2.SelectionChanged, Printer3.SelectionChanged, Printer4.SelectionChanged
        Dim cmbbox = DirectCast(sender, ComboBox)
        Select Case cmbbox.Name
            Case "Printer1"
                If Printer1.SelectedIndex <> 2 Then
                    MsgBox("Not available in this release")
                    Printer1.SelectedIndex = 2
                End If
                p1label.Opacity = 0
                p1type = Convert.ToString(Printer1.SelectedValue)
                My.Settings.Printer1 = Printer1.SelectedIndex
            Case "Printer2"
                If Printer2.SelectedIndex <> 2 Then
                    MsgBox("Not available in this release")
                    Printer2.SelectedIndex = 2
                End If
                p2label.Opacity = 0
                p2type = Convert.ToString(Printer2.SelectedValue)
                My.Settings.Printer2 = Printer2.SelectedIndex
            Case "Printer3"
                p3label.Opacity = 0
                p3type = Convert.ToString(Printer3.SelectedValue)
                My.Settings.Printer3 = Printer3.SelectedIndex
            Case "Printer4"
                p4label.Opacity = 0
                p4type = Convert.ToString(Printer4.SelectedValue)
                My.Settings.Printer4 = Printer4.SelectedIndex
        End Select
    End Sub
    Private Sub DataFile_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles DataFile.SelectionChanged
        NGPCLBuilder.CancelAsync()
        DF_label.Opacity = 0
        DFile = DataFile.SelectedValue
        My.Settings.Datafile = DataFile.SelectedIndex
        CodeCount = File.ReadAllLines(DB_Path & DFile).Length
        Remaining_.Text = CodeCount
        filepos = 0
        If CodeCount < 101 Then
            Remaining_.Foreground = OrangeBrush
        Else Remaining_.Foreground = BlackBrush
        End If
        If firstload = False Then
            If NGPCL.Connected <> True Then
                Try
                    NGPCL.Connect(p1IPstr, p1PORTstr)
                    Dim writer As New System.IO.BinaryWriter(NGPCL.GetStream())
                    Dim Reader As New System.IO.BinaryReader(NGPCL.GetStream)
                    writer.Write("{~ER|External_0|}")
                    P_ACK = Reader.ReadBytes(7)
                    Console.WriteLine("clear buffer response " & ByteArrayToString(P_ACK))
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    CommsFault = True
                    P1BG.Background = RedBrush
                    Start.IsEnabled = 1
                    StopB.IsEnabled = 0
                End Try
            Else
                Try
                    Dim writer As New System.IO.BinaryWriter(NGPCL.GetStream())
                    Dim Reader As New System.IO.BinaryReader(NGPCL.GetStream)
                    writer.Write("{~ER|External_0|}")
                    P_ACK = Reader.ReadBytes(7)
                    Console.WriteLine("clear buffer response " & ByteArrayToString(P_ACK))
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    ConnectError = 1
                    BG.Background = RedBrush
                End Try
            End If
        End If
        READER()
        New_Batch()
        If Change = True Then
            Running.RunWorkerAsync(1)
        End If
    End Sub
    Private Sub TextBox_TextChanged(sender As Object, e As TextChangedEventArgs)
    End Sub
    Private Sub Start_Click(sender As Object, e As RoutedEventArgs) Handles Start.Click
        'My.Settings.Save()
        If p1IP.Text <> Nothing And p1PORT.Text <> Nothing And DataFile.SelectedValue <> Nothing And DBList.Count <> 0 Then
            Start.IsEnabled = 0
            StopB.IsEnabled = 1
            If NGPCL.Connected <> True Then
                Try
                    NGPCL.Connect(p1IPstr, p1PORTstr)
                    P1BG.Background = GreenBrush
                    Running.RunWorkerAsync(1)
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    CommsFault = True
                    P1BG.Background = RedBrush
                    Start.IsEnabled = 1
                    StopB.IsEnabled = 0
                End Try
            Else
                P1BG.Background = GreenBrush
                If NGPCLBuilder.IsBusy = True Then
                    BG.Background = GreenBrush
                Else
                    Running.RunWorkerAsync(1)
                End If
            End If
        End If
        firstload = False
    End Sub

    Private Sub p1IP_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p1IP.TextChanged
        p1IPstr = p1IP.Text
        My.Settings.P1IP = p1IP.Text
    End Sub

    Private Sub Reset_Click(sender As Object, e As RoutedEventArgs) Handles reset.Click
        filepos = 0
        Remaining_.Text = CodeCount
        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Normal, New Action(AddressOf New_Batch))
        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Update_count))
    End Sub

    Private Sub Start_2_Click(sender As Object, e As RoutedEventArgs) Handles Start_2.Click
        'My.Settings.Save()
        If p2IP.Text <> Nothing And p2PORT.Text <> Nothing And DataFile_2.SelectedValue <> Nothing Then
            Start_2.IsEnabled = 0
            StopB_2.IsEnabled = 1
            If NGPCL2.Connected <> True Then
                Try
                    NGPCL2.Connect(p2IPstr, p2PORTstr)
                    P2BG.Background = GreenBrush
                    Running.RunWorkerAsync(2)
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    CommsFault = True
                    P2BG.Background = RedBrush
                    Start_2.IsEnabled = 1
                    StopB_2.IsEnabled = 0
                End Try
            Else
                P2BG.Background = GreenBrush
                If NGPCLBuilder.IsBusy = True Then
                    BG.Background = GreenBrush
                Else
                    Running.RunWorkerAsync(2)
                End If
            End If
        End If
        firstload = False
    End Sub

    Private Sub StopB_2_Click(sender As Object, e As RoutedEventArgs) Handles StopB_2.Click
        BG.Background = OrangeBrush
        Dim dbpath As String = DB_Path & DataFile.SelectedValue
        Running.CancelAsync()
        _90xxBuilder.CancelAsync()
        StopB_2.IsEnabled = 0
        Start_2.IsEnabled = 1
        My.Settings.Datafile2 = DataFile_2.SelectedIndex
        My.Settings.Printer2 = Printer2.SelectedIndex
        My.Settings.P2IP = p2IP.Text
        My.Settings.P2PORT = p2PORT.Text
        My.Settings.Save()
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Try
            Source_Folder = Process.Start("C:\Program files\Mitech\Mipromo\Source Folder.exe")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Source_Folder.WaitForExit()
        DB_Path = File.ReadAllText("C:\Program Files\Mitech\MiPromo\Data Store\Source.mit")
        For Each file As String In System.IO.Directory.GetFiles(DB_Path)
            DataFile.Items.Add(System.IO.Path.GetFileName(file))
        Next
        For i = 0 To DataFile.Items.Count - 1
            DataFile_2.Items.Add(DataFile.Items(i))
        Next
    End Sub

    Private Sub Button_Click_1(sender As Object, e As RoutedEventArgs)
        Try
            Process.Start("C:\Program files\Mitech\Mipromo\Data Manager\Database Upload.exe")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DataFile_2_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles DataFile_2.SelectionChanged
        DF_label_2.Opacity = 0
        DFile2 = DataFile_2.SelectedValue
        My.Settings.Datafile2 = DataFile_2.SelectedIndex
        CodeCount = File.ReadAllLines(DB_Path & DFile2).Length
        Remaining_.Text = CodeCount
        If CodeCount < 101 Then
            Remaining_.Foreground = OrangeBrush
        Else Remaining_.Foreground = BlackBrush
        End If
        If firstload = False Then
            If NGPCL2.Connected <> True Then
                Try
                    NGPCL2.Connect(p1IPstr, p1PORTstr)
                    Dim writer2 As New System.IO.BinaryWriter(NGPCL2.GetStream())
                    Dim Reader2 As New System.IO.BinaryReader(NGPCL2.GetStream)
                    writer2.Write("{~ER|External_0|}")
                    P_ACK = Reader2.ReadBytes(7)
                    Console.WriteLine("clear buffer response " & ByteArrayToString(P_ACK))
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    CommsFault = True
                    P2BG.Background = RedBrush
                    Start_2.IsEnabled = 1
                    StopB_2.IsEnabled = 0
                End Try
            Else
                Try
                    Dim writer2 As New System.IO.BinaryWriter(NGPCL2.GetStream())
                    Dim Reader2 As New System.IO.BinaryReader(NGPCL2.GetStream)
                    writer2.Write("{~ER|External_0|}")
                    P_ACK = Reader2.ReadBytes(7)
                    Console.WriteLine("clear buffer response " & ByteArrayToString(P_ACK))
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    ConnectError = 1
                    BG.Background = RedBrush
                End Try
            End If
        End If
        READER()
        New_Batch()
        If Change = True Then
            Running.RunWorkerAsync(2)
        End If
    End Sub

    Private Sub p2IP_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p2IP.TextChanged
        p2IPstr = p2IP.Text
        My.Settings.P2IP = p2IP.Text
    End Sub
    Private Sub p3IP_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p3IP.TextChanged
        p3IPstr = p3IP.Text
        My.Settings.P3IP = p3IP.Text
    End Sub
    Private Sub p4IP_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p4IP.TextChanged
        p4IPstr = p4IP.Text
        My.Settings.P4IP = p4IP.Text
    End Sub
    Private Sub Message_Name_TextChanged_1(sender As Object, e As TextChangedEventArgs)
        message = Message_Name.Text
        My.Settings.Message = Message_Name.Text
    End Sub
    Private Sub StopB_Click(sender As Object, e As RoutedEventArgs) Handles StopB.Click
        BG.Background = OrangeBrush
        Dim dbpath As String = DB_Path & DataFile.SelectedValue
        Running.CancelAsync()
        _90xxBuilder.CancelAsync()
        StopB.IsEnabled = 0
        Start.IsEnabled = 1
        My.Settings.FilePos = filepos
        My.Settings.Datafile = DataFile.SelectedIndex
        My.Settings.Printer1 = Printer1.SelectedIndex
        My.Settings.P1IP = p1IP.Text
        My.Settings.P1PORT = p1PORT.Text
        My.Settings.Save()
    End Sub
    Private Sub p1PORT_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p1PORT.TextChanged
        p1PORTstr = p1PORT.Text
        My.Settings.P1PORT = p1PORT.Text
    End Sub
    Private Sub p2PORT_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p2PORT.TextChanged
        p2PORTstr = p2PORT.Text
        My.Settings.P2PORT = p2PORT.Text
    End Sub
    Private Sub p3PORT_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p3PORT.TextChanged
        p3PORTstr = p3PORT.Text
        My.Settings.P3PORT = p3PORT.Text
    End Sub
    Private Sub p4PORT_TextChanged(sender As Object, e As TextChangedEventArgs) Handles p4PORT.TextChanged
        p4PORTstr = p4PORT.Text
        My.Settings.P4PORT = p4PORT.Text
    End Sub
    Private Sub p1IP_Gotfocus(sender As Object, e As RoutedEventArgs) Handles p1IP.GotFocus, p1PORT.GotFocus, p2IP.GotFocus, p2PORT.GotFocus, p3IP.GotFocus, p3PORT.GotFocus, p4IP.GotFocus, p4PORT.GotFocus, Message_Name.GotFocus
        Dim txtbx = DirectCast(sender, TextBox)
        Select Case txtbx.Name
            Case "p1IP"
                P1ipLabel.Opacity = 0
            Case "p1PORT"
                p1portLabel.Opacity = 0
            Case "p2IP"
                P2ipLabel.Opacity = 0
            Case "p2PORT"
                p2portLabel.Opacity = 0
            Case "p3IP"
                P3ipLabel.Opacity = 0
            Case "p3PORT"
                p3portLabel.Opacity = 0
            Case "p4IP"
                P4ipLabel.Opacity = 0
            Case "p4PORT"
                p4portLabel.Opacity = 0
        End Select
    End Sub
    Private Sub p1IP_LostFocus(sender As Object, e As RoutedEventArgs) Handles p1IP.LostFocus, p1PORT.LostFocus, p2IP.LostFocus, p2PORT.LostFocus, p3IP.LostFocus, p3PORT.LostFocus, p4IP.LostFocus, p4PORT.LostFocus, Message_Name.LostFocus
        Dim txtbx = DirectCast(sender, TextBox)
        If Trim(txtbx.Text) <> vbNullString Then
            Select Case txtbx.Name
                Case "p1IP"
                    P1ipLabel.Opacity = 0
                Case "p1PORT"
                    p1portLabel.Opacity = 0
                Case "p2IP"
                    P2ipLabel.Opacity = 0
                Case "p2PORT"
                    p2portLabel.Opacity = 0
                Case "p3IP"
                    P3ipLabel.Opacity = 0
                Case "p3PORT"
                    p3portLabel.Opacity = 0
                Case "p4IP"
                    P4ipLabel.Opacity = 0
                Case "p4PORT"
                    p4portLabel.Opacity = 0
            End Select
        Else
            Select Case txtbx.Name
                Case "p1IP"
                    P1ipLabel.Opacity = 1
                Case "p1PORT"
                    p1portLabel.Opacity = 1
                Case "p2IP"
                    P2ipLabel.Opacity = 1
                Case "p2PORT"
                    p2portLabel.Opacity = 1
                Case "p3IP"
                    P3ipLabel.Opacity = 1
                Case "p3PORT"
                    p3portLabel.Opacity = 1
                Case "p4IP"
                    P4ipLabel.Opacity = 1
                Case "p4PORT"
                    p4portLabel.Opacity = 1
            End Select
        End If
    End Sub
    Private Sub Running_DoWork(sender As Object, e As DoWorkEventArgs) Handles Running.DoWork
        Console.WriteLine("argument = " & e.Argument)
        If next_message = "" Then
            next_message = String.Join("|", DBList)
            Console.WriteLine("New message string:" & next_message)
            If DBList.Count <> 0 Then
                next_message = String.Join("|", DBList)
            Else MsgBox("No more codes in this database!")
                If e.Argument = 1 Then
                    P1BG.Background = RedBrush
                    Running.CancelAsync()
                    NGPCLBuilder.CancelAsync()
                    StopB.IsEnabled = 0
                    Start.IsEnabled = 1
                ElseIf e.Argument = 2 Then
                    P2BG.Background = RedBrush
                    Running.CancelAsync()
                    NGPCLBuilder.CancelAsync()
                    StopB_2.IsEnabled = 0
                    Start_2.IsEnabled = 1
                End If
                If Running.CancellationPending Then
                    e.Cancel = True
                End If
            End If
        End If
        e.Result = e.Argument
    End Sub
    Private Sub Running_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Running.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            '' if BackgroundWorker terminated due to error
            MessageBox.Show(e.Error.Message)
        ElseIf e.Cancelled = True Or StopB.IsEnabled = 0 Then
            Try
                Running.Dispose()
            Catch ex As Exception
                MsgBox("Communication in progress, please wait.")
            End Try
        End If

        If e.Result = 1 Then
            Try
                Remainder = CodeCount - filepos
                Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Update_count))
                NGPCLBuilder.RunWorkerAsync(1)
            Catch ex As Exception
                MsgBox("Current code not printed yet.")
            End Try
        ElseIf e.Result = 2 Then
            Try
                Remainder = CodeCount - filepos
                Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Update_count))
                NGPCLBuilder.RunWorkerAsync(2)
            Catch ex As Exception
                MsgBox("Current code not printed yet.")
            End Try
        End If
    End Sub
    Private Sub NGPCLBuilder_DoWork(sender As Object, e As DoWorkEventArgs) Handles NGPCLBuilder.DoWork
        If next_message <> Nothing Then
            If e.Argument = 1 Then
                Dim MSGSTR As String = "{~EA|External_0|" & next_message & "|}"
                File.AppendAllText(DB_Path & "Logs\" & DFile & ".log", ("Codes: " & Strings.Left(next_message, 12) & " ==> " & Right(next_message, 12) & " Sent to printer " & e.Argument & ": " & DateString & " " & TimeString & Environment.NewLine))
                Try
                    Dim writer As New System.IO.BinaryWriter(NGPCL.GetStream())
                    Dim Reader As New System.IO.BinaryReader(NGPCL.GetStream)
                    Console.WriteLine("next message: " & next_message)
                    writer.Write(MSGSTR)
                    writer.Flush()
                    P_ACK = Reader.ReadBytes(7)
                    Dim PSTR As String = ByteArrayToString(P_ACK)
                    If PSTR = "7B7E4541307C7D" Then
                        ConnectError = 0
                        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Normal, New Action(AddressOf GTG))
                        Do
                            Try
                                writer.Write(request)
                                P_ACK = Reader.ReadBytes(12)
                                P_ACKstr = ByteArrayToString(P_ACK)
                                If P_ACK.Count = 12 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 12)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    Dim ACK3 As String = Strings.Left(ACK1, 4)
                                    Dim ACK4 As String = Right(ACK3, 2)
                                    Dim ACK5 As String = Strings.Left(ACK1, 6)
                                    Dim ACK6 As String = Right(ACK5, 2)
                                    Dim ACK7 As String = Strings.Left(ACK1, 8)
                                    Dim ACK8 As String = Right(ACK7, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30 & Convert.ToInt32(ACK4) - 30 & Convert.ToInt32(ACK6) - 30 & Convert.ToInt32(ACK8) - 30)
                                ElseIf P_ACK.Count = 11 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 11)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    Dim ACK3 As String = Strings.Left(ACK1, 4)
                                    Dim ACK4 As String = Right(ACK3, 2)
                                    Dim ACK5 As String = Strings.Left(ACK1, 6)
                                    Dim ACK6 As String = Right(ACK5, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30 & Convert.ToInt32(ACK4) - 30 & Convert.ToInt32(ACK6))
                                ElseIf P_ACK.Count = 10 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 10)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    Dim ACK3 As String = Strings.Left(ACK1, 4)
                                    Dim ACK4 As String = Right(ACK3, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30 & Convert.ToInt32(ACK4))
                                ElseIf P_ACK.Count = 9 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 9)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30)
                                End If
                                Console.WriteLine("Codes now remaining in buffer: " & b_count)
                            Catch sockex As Net.Sockets.SocketException
                                MessageBox.Show(sockex.Message)
                                ConnectError = 1
                                Exit Do
                            End Try
                            If NGPCLBuilder.CancellationPending = True Then
                                Exit Do
                            End If
                        Loop Until b_count <= 100
                    Else NGPCLBuilder.CancelAsync()
                        MessageBox.Show("Buffer failed to update, Please check printer")
                    End If
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    ConnectError = 1
                    StopB.IsEnabled = 0
                    Start.IsEnabled = 1
                End Try
            ElseIf e.Argument = 2 Then
                Dim MSGSTR As String = "{~EA|External_0|" & next_message & "|}"
                File.AppendAllText(DB_Path & "Logs\" & DFile & ".log", ("Codes: " & Strings.Left(next_message, 12) & " ==> " & Right(next_message, 12) & " Sent to printer " & e.Argument & ": " & DateString & " " & TimeString & Environment.NewLine))
                Try
                    Dim writer2 As New System.IO.BinaryWriter(NGPCL2.GetStream())
                    Dim Reader2 As New System.IO.BinaryReader(NGPCL2.GetStream)
                    writer2.Write(MSGSTR)
                    writer2.Flush()
                    P_ACK = Reader2.ReadBytes(7)
                    Dim PSTR As String = ByteArrayToString(P_ACK)
                    If PSTR = "7B7E4541307C7D" Then
                        ConnectError = 0
                        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Normal, New Action(AddressOf GTG))
                        Do
                            Try
                                writer2.Write(request)
                                P_ACK = Reader2.ReadBytes(12)
                                P_ACKstr = ByteArrayToString(P_ACK)
                                If P_ACK.Count = 12 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 12)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    Dim ACK3 As String = Strings.Left(ACK1, 4)
                                    Dim ACK4 As String = Right(ACK3, 2)
                                    Dim ACK5 As String = Strings.Left(ACK1, 6)
                                    Dim ACK6 As String = Right(ACK5, 2)
                                    Dim ACK7 As String = Strings.Left(ACK1, 8)
                                    Dim ACK8 As String = Right(ACK7, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30 & Convert.ToInt32(ACK4) - 30 & Convert.ToInt32(ACK6) - 30 & Convert.ToInt32(ACK8) - 30)
                                ElseIf P_ACK.Count = 11 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 11)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    Dim ACK3 As String = Strings.Left(ACK1, 4)
                                    Dim ACK4 As String = Right(ACK3, 2)
                                    Dim ACK5 As String = Strings.Left(ACK1, 6)
                                    Dim ACK6 As String = Right(ACK5, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30 & Convert.ToInt32(ACK4) - 30 & Convert.ToInt32(ACK6))
                                ElseIf P_ACK.Count = 10 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 10)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    Dim ACK3 As String = Strings.Left(ACK1, 4)
                                    Dim ACK4 As String = Right(ACK3, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30 & Convert.ToInt32(ACK4))
                                ElseIf P_ACK.Count = 9 Then
                                    Dim ACK1 As String = Right(P_ACKstr, 9)
                                    Dim ACK2 As String = Strings.Left(ACK1, 2)
                                    b_count = (Convert.ToInt32(ACK2) - 30)
                                End If
                                Console.WriteLine("Codes now remaining in buffer: " & b_count)
                            Catch sockex As Net.Sockets.SocketException
                                MessageBox.Show(sockex.Message)
                                ConnectError = 1
                                Exit Do
                            End Try
                        Loop Until b_count <= 1020
                    Else NGPCLBuilder.CancelAsync()
                        MessageBox.Show("Buffer failed to update, Please check printer")
                    End If
                Catch sockex As Net.Sockets.SocketException
                    MessageBox.Show(sockex.Message)
                    ConnectError = 1
                    StopB_2.IsEnabled = 0
                    Start_2.IsEnabled = 1
                End Try
            End If
            If ConnectError = 1 Then
                Running.CancelAsync()
                NGPCLBuilder.CancelAsync()
                _90xxBuilder.CancelAsync()
                StopB.IsEnabled = 0
                Start.IsEnabled = 1
                StopB_2.IsEnabled = 0
                Start_2.IsEnabled = 1
            End If
        End If
        e.Result = e.Argument
    End Sub
    Private Sub NGPCLBuilder_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles NGPCLBuilder.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            '' if BackgroundWorker terminated due to error
            MessageBox.Show(e.Error.Message)
            NGPCLBuilder.Dispose()
        ElseIf e.Cancelled Then
            NGPCLBuilder.Dispose()
        Else
            If e.Result = 1 Then
                If b_count <= 1020 Then
                    UpdateApplicationDataUI(1)
                End If
            ElseIf e.Result = 2 Then
                If b_count <= 1020 Then
                    UpdateApplicationDataUI(2)
                End If
            End If
        End If
    End Sub
    Private Function ByteArrayToString(ByVal arrInput() As Byte) As String
        Dim i As Integer
        Dim sOutput As New StringBuilder(arrInput.Length)
        For i = 0 To arrInput.Length - 1
            sOutput.Append(arrInput(i).ToString("X2"))
        Next
        Return sOutput.ToString()
    End Function
    Private Sub UpdateApplicationDataUI(ByVal sender As Integer)
        Running.CancelAsync()
        NGPCLBuilder.CancelAsync()
        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Normal, New Action(AddressOf New_Batch))
        If sender = 1 Then
            Running.RunWorkerAsync(1)
        ElseIf sender = 2 Then
            Running.RunWorkerAsync(2)
        End If
    End Sub

    Public Function READER()
        DFile = DataFile.SelectedValue
        Using DBReader As New StreamReader(DB_Path & DFile)
            While DBReader.EndOfStream = False
                Buffer.Add(DBReader.ReadLine())
            End While
        End Using
        Return Buffer
    End Function
    Public Function New_Batch()
        next_message = ""
        DBList.Clear()
        Do While DBList.Count < 1500
            DBList.Add(Buffer(filepos))
            filepos += 1
        Loop
        Return filepos
    End Function
    Private Sub GTG()
        BG.Background = GreenBrush
    End Sub
    Private Sub Update_count()
        Remaining_.Text = Remainder
    End Sub

    Private Sub MainWindow_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If Buffer.Count <> 0 Or DBList.Count <> 0 Then
            filepos = filepos - 1500
        End If
        My.Settings.FilePos = filepos
        My.Settings.Datafile = DataFile.SelectedIndex
        My.Settings.Printer1 = Printer1.SelectedIndex
        My.Settings.P1IP = p1IP.Text
        My.Settings.P1PORT = p1PORT.Text
        My.Settings.Datafile2 = DataFile_2.SelectedIndex
        My.Settings.Printer2 = Printer2.SelectedIndex
        My.Settings.P2IP = p2IP.Text
        My.Settings.P2PORT = p2PORT.Text
        My.Settings.Save()
    End Sub
    Public Sub Monthly_download_DoWork(sender As Object, e As DoWorkEventArgs) Handles Monthly_download.DoWork
        Dim dtNow As DateTime = DateTime.Now
        Dim dc As Integer
        Change = False
        If dtNow.Month = 2 Then
            dc = 28
        ElseIf dtNow.Month = 4 Or 6 Or 9 Or 11 Then
            dc = 30
        Else
            dc = 31
        End If
        Do
            If DateString = dtNow.Month & "-" & dc & "-" & dtNow.Year And TimeString = "23:59:00" Then
                Change = True
            Else Change = False
            End If
        Loop Until Change = True
    End Sub
    Private Sub Monthly_download_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Monthly_download.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            '' if BackgroundWorker terminated due to error
            MessageBox.Show(e.Error.Message)
        ElseIf e.Cancelled Then
            Monthly_download.Dispose()
        End If
        If Change = True Then
            Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf DB_Change))
        End If
    End Sub
    Private Function DB_Change()
        NGPCLBuilder.CancelAsync()
        Try
            Dim writer As New System.IO.BinaryWriter(NGPCL.GetStream())
            Dim Reader As New System.IO.BinaryReader(NGPCL.GetStream)
            writer.Write("{~ST|11|}")
            P_ACK = Reader.ReadBytes(7)
        Catch sockex As Net.Sockets.SocketException
            MessageBox.Show(sockex.Message)
            ConnectError = 1
        End Try
        If Month(Now) <> 12 Then
            If Month(Now) < 10 Then
                DataFile.SelectedValue = "rfider-" & Year(Now) & "-0" & (Month(Now) + 1) & ".csv"
            Else
                DataFile.SelectedValue = "rfider-" & Year(Now) & "-" & (Month(Now) + 1) & ".csv"
            End If
        Else
            DataFile.SelectedValue = "rfider-" & Year(Now) & "-" & 1 & ".csv"
        End If
        Monthly_download.RunWorkerAsync()
    End Function

End Class
